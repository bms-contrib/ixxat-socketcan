This repo is used for staging the patches that are used in the AUR package [here](https://gitlab.com/bms-contrib/arch-linux-pkg/ixxat-socketcan), which always fetches the code from the official channels. This repo is _not_ kept in sync with the official source code as long as the generated patches still apply on it and it compiles against a recent kernel.

Official packages should be taken from IXXAT's website.

I am not and have never been affiliated with IXXAT.
